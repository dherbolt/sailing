## Pre-cooked
* https://www.hotovky.cz/sklenice
  * package/person
  * 80 KC/portion (2019) 


## Cooking
### DAY 1 sour lentils:
* lentils		(100g/person)
* onion
* roux (jiska)
* sunflower oil
* butter
* vinegar
* sausages (smoked meat)

### DAY 2 pasta aglio e olio:
* linguine	(100g/person)
* garlic
* olive oil
* chilli
* parmigiano
 
### DAY 3 rizoto:
* Rizoto:
* Rice		(100g/person)
* pees		(canned/frozen)
* chicken		(200g/person)
 
### DAY 4 potatoe mash:
* potatoes	(300g/person)
* meatloaf    (200g/person)
* milk
* butter
 
### DAY 5 chilli con carne:
* kidney beans
* tomatoe puree
* peppers
* onion

 
### DAY 6 pasta pesto:
* linguine	(100g/person)
* basil pesto
* ground pepper
* olive oil 
* onion
 
### DAY 7 mac and cheese:
* elbow pasta (100g/person)
* butter
* flour
* salt
* ground black pepper 
* heavy cream 
* cheddar 
* cream cheese

### Daily:
#### Greek salad:
* red onions 
* cucumber
* peppers 
* olives
* feta
 
#### Potaoe salad:
* potatoes
* lemon
* salt
* ground pepper
* parsley
* onion
* tomatoes

